const http = require('http');
const fs = require('fs');

// Server configuration
const hostname = '127.0.0.1';
const port = 3000;

console.log(__dirname)
// Render page
fs.readFile('../../../helper/templates/index.html', function (err, html) {
    
    if (err) {
        throw err}
    
    //const server = http.createServer(function(request, response) { 
    http.createServer(function(request, response) {  
        response.writeHeader(200, {"Content-Type": "text/html"});  
        response.write(html);  
        response.end();  
    })
    .listen(port, hostname, () => {
    	// Console
	  	console.log(`Server running at http://${hostname}:${port}/`);
	});

    //console.log(server)
    
});